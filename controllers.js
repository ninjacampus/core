import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export const loginController = ({
  model,
  loginFields = ["email"],
  hashedLoginFields = ["password"],
  fieldsToKeep = [],
  role,
}) => async (req, res) => {
  // Check if fields exist in body
  loginFields.forEach((field) => {
    if (req.body[field] === undefined) {
      return res.status(401).json("Bad credentials");
    }
  });

  // Fetch user by login fields
  const user = await model.findOne({
    where: loginFields.reduce((acc, cur) => {
      acc[cur] = req.body[cur];
      return acc;
    }, {}),
  });

  if (!user) {
    return res.status(401).json({ error: "Bad credentials" });
  }

  if (role && user.role !== role) {
    return res.status(401).json({ error: "Invalid role" });
  }

  // Passwords are correct
  const isAuthorized =
    hashedLoginFields.filter((f) => !bcrypt.compareSync(req.body[f], user[f]))
      .length === 0;

  if (!isAuthorized) {
    return res.status(401).json({ error: "Bad credentials" });
  }

  // Store fields to keep in jwt payload
  fieldsToKeep = [...new Set(["id", "role", ...fieldsToKeep])];
  const fieldsToSave = fieldsToKeep.reduce((acc, curr) => {
    if (user[curr]) {
      acc[curr] = user[curr];
    }
    return acc;
  }, {});

  const resp = {
    access_token: jwt.sign(fieldsToSave, req.app.locals.jwt_secret, {
      expiresIn: 60 * 60 * 4,
    }),
    user,
  };

  return res.status(200).json(resp);
};
