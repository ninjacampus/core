let _config = null;

export const createConfig = (config) => {
  _config = config;
};

export default _config;
