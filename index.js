import { createConfig } from "./config.js";
import server from "./server.js";

const NCore = (config) => {
  createConfig(config);
  return server(config);
};

export { default as createDb } from "./db.js";
export { oapi } from "./openapi.js";
export { loginController } from "./controllers.js";
export { authMiddleware } from "./middlewares/auth.middleware.js";
export { roleMiddleware } from "./middlewares/role.middleware.js";
export { validatorMiddleware } from "./middlewares/validator.middleware.js";
export default NCore;
