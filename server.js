import express from "express";
import logger from "morgan";
import bodyParser from "body-parser";
import cors from "cors";

import oapi from "./openapi.js";

const server = (config) => {
  const app = express();

  // Parse incoming requests data (https://github.com/expressjs/body-parser)
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  if (config.environment === "development") {
    // Log requests to the console.
    app.use(logger("dev"));

    // CORS handling
    app.use(cors());
  }
  if (config.doc.serve === "true") {
    // OpenApi
    app.use(oapi);
    app.use(config.doc.path, oapi.swaggerui);
  }

  // Routes for static files
  app.use("/uploads", express.static("uploads"));

  app.use((err, req, res, next) => {
    res.status(err.status || 500).json({
      error: err.message,
      validation: err.validationErrors,
      schema: err.validationSchema,
    });
  });

  Object.keys(config).forEach((c) => (app.locals[c] = config[c]));

  app.run = () => {
    try {
      app.listen(config.app.port, () => {
        console.log("\nApplication listening on port " + config.app.port);
      });
    } catch (err) {
      console.log(err);
      process.exit(-1);
    }
  };

  return app;
};

export default server;
