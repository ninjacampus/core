import jwt from "jsonwebtoken";

export const authMiddleware = (req, res, next) => {
  /*
   * Check if authorization header is set
   */
  if (
    req.hasOwnProperty("headers") &&
    req.headers.hasOwnProperty("authorization")
  ) {
    try {
      /*
       * Try to decode & verify the JWT token
       * The token contains user's id ( it can contain more informations )
       * and this is saved in req.user object
       */
      const bearerHeader = req.headers["authorization"];
      if (bearerHeader) {
        let token = "";
        if (bearerHeader.startsWith("Bearer")) {
          [, token] = bearerHeader.split(" ");
        } else {
          token = bearerHeader;
        }
        req.user = jwt.verify(token, req.app.locals.jwt_secret);
      }
    } catch (err) {
      /*
       * If the authorization header is corrupted, it throws exception
       * So return 401 status code with JSON error message
       */
      return res.status(401).json({
        error: {
          msg: "Invalid authorization token",
        },
      });
    }
  } else {
    /*
     * If there is no autorization header, return 401 status code with JSON
     * error message
     */
    return res.status(401).json({
      error: {
        msg: "Missing authorization token",
      },
    });
  }
  next();
  return;
};
