import openapi from "@express/openapi";

export const oapi = openapi({
  openapi: "3.0.0",
  info: {
    title: "Jungler Backend Application",
    description: "Documentation automatically generated from REST API",
    version: "1.0.0",
  },
  components: {
    securitySchemes: {
      BearerAuth: {
        type: "http",
        scheme: "bearer",
        bearerFormat: "JWT",
      },
    },
  },
});

export default oapi;
