import Sequelize from "sequelize";

export const createDb = (config, resources) => {
  const db = {};

  const sequelize = new Sequelize(
    config.db.name,
    config.db.username,
    config.db.password,
    config.db
  );

  console.log(config.db);

  db.sequelize = sequelize;
  db.Sequelize = Sequelize;

  resources.forEach((resource) => {
    const model = resource(sequelize, Sequelize.DataTypes);
    db[model.name] = sequelize.models[model.name];
  });

  Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
      db[modelName].associate(db);
    }
  });

  return db;
};

export default createDb;
